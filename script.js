let cabecalho = document.querySelector(".cabecalho > p")
cabecalho.innerHTML = 'Ajude o gatinho Nyan a encontrar o arco-íris!';

let comentario = document.querySelector(".comentario > p")
comentario.innerHTML = 'Use as setas do teclado para percorrer o labirinto.';

const somVitoria = new Audio("img/nyan-cat_1.wav"); 


'use strict';
const labirinto = document.getElementById("background");
let horizontal = 0;
let vertical = 9;

const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

for (let l = 0; l < map.length; l++) {
    let linha = document.createElement("div");
    linha.classList.add("linha");

    for (let c = 0; c < map[l].length; c++) {

        let id_str = l + "." + c;
        switch (map[l][c]) {
            case "F":
                let fim = document.createElement("div");
                fim.classList.add("fim", "labirinto");
                fim.id = id_str;
                linha.appendChild(fim);
                break;
            case "W":
                let parede = document.createElement("div");
                parede.classList.add("parede", "labirinto");
                parede.id = id_str;
                linha.appendChild(parede);
                break;
            case "S":
                let comeco = document.createElement("div");
                comeco.classList.add("comeco", "labirinto");
                comeco.id = id_str;
                linha.appendChild(comeco);
                break;
            case " ":
                let vazio = document.createElement("div");
                vazio.classList.add("vazio", "labirinto");
                vazio.id = id_str;
                linha.appendChild(vazio);
        }
    }
    labirinto.appendChild(linha);
}

function verifica1() {

    let curLoc = document.getElementById("usuario");
    curLoc.parentNode.removeChild(curLoc);

    let id_str = vertical + "." + horizontal;
    let newDiv = document.getElementById(id_str);
    newDiv.appendChild(curLoc);
}
verifica1();

document.addEventListener('keydown', movimento);

function verifica2(x, y) {
    return (
        y >= 0 &&
        y < map.length &&
        x >= 0 &&
        x < map[0].length &&
        map[y][x] !== "W"
    )
}

function movimento(event) {
    switch (event.keyCode) {
        case 40:
            if (verifica2(horizontal, vertical + 1)) {
                vertical += 1;
            }
            break;

        case 38:
            if (verifica2(horizontal, vertical - 1)) {
                vertical -= 1;
            }
            break;

        case 37:
            if (verifica2(horizontal - 1, vertical)) {
                horizontal -= 1;
            }
            break;

        case 39:
            if (verifica2(horizontal + 1, vertical)) {
                horizontal += 1;
            }
            break;
    }
 

    verifica1();

    if (map[vertical][horizontal] == "F") {
        document.getElementById("vitoria").src = "img/giphy.gif" ;
        somVitoria.play();
    }
}
